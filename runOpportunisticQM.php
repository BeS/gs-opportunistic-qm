<?php

$lockfile = '/home/schiesbn/gnusocialQueueRuns.lock';

$curl = curl_init();
curl_setopt_array($curl, array(
    CURLOPT_RETURNTRANSFER => 1,
    CURLOPT_URL => 'https://io.schiessle.org/main/runqueue'
));

$run = !file_exists($lockfile);

file_put_contents($lockfile, 'lock');

while ($run === true) {
    $result = curl_exec($curl);
    if ($result === '0') {
        $run = false;
    }
}

unlink($lockfile);
